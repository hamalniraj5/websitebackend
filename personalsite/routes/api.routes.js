const router = require('express').Router();
const authRoute = require('../controller/auth.route');
const projectRoute = require('../modules/projects/projects.route');
const authenticate = require('../middlewares/authenticate');

router.use('/auth', authRoute);
router.use('/projects',authenticate, projectRoute);


module.exports = router;