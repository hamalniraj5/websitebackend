var express = require('express');
const path = require('path');
const morgan = require('morgan');
const cors = require('cors');

var app = express();


app.set('port', 9090);
var port = app.get('port');

require('./db');

app.use(cors());
app.use(morgan('dev'));

app.use(express.static('files'));
app.use('/file', express.static(path.join(__dirname, 'files')));

app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

const apiRoute = require('../personalsite/routes/api.routes');
app.use('/api', apiRoute);

app.use(function (req, res, next) {
    next({
        msg: "Not Found",
        status: 404
    })
})

app.use(function (err, req, res, next) {
    console.log("i am also middleware");
    res
        .status(err.status || 400)
        .json({
            msg: err.msg || err,
            status: err.status || 404
        })
})

app.listen(port, function (err, done) {
    if (err) {
        console.log('Server connection Failed');
    } else {
        console.log('server connection successfull');
    }
})