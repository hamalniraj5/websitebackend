const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const projectSchema = new Schema({
    projectName: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['Completed', 'Ongoing']
    },
    image: String
}, {
    timestamps: true

});

module.exports = mongoose.model('projects', projectSchema);