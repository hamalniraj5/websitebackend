const ProductModel = require('../projects/projects.model');

function map_project_req(project, projectDetails) {
    if (projectDetails.projectName)
        project.projectName = projectDetails.projectName;
    if (projectDetails.description)
        project.description = projectDetails.description;
    if (projectDetails.status)
        project.status = projectDetails.status;
    if (projectDetails.image)
        project.image = projectDetails.image;
}

function insert(data) {
    var newProject = new ProductModel({});
    map_project_req(newProject, data);
    return newProject.save()

}

function find(condition) {
    return ProductModel
        .find(condition)
        .sort({
            _id: -1
        })
        .exec();
}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id)
            .then(function (project) {
                if (!project) {
                    return reject({
                        msg: "project not Found"
                    })
                }
                map_project_req(project, data);
                project.save(function (err, done) {
                    if (err) {
                        reject(err);
                    }
                    resolve(done);
                })

            })
            .catch(function (err) {
                reject(err);
            })
    })

}

function remove(id) {
    return ProductModel.findByIdAndRemove(id);
}

module.exports = {
    insert,
    find,
    update,
    remove
}
