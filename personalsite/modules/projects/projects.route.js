const router = require('express').Router();
const ProductCntrl = require('../projects/projects.controller');
const Uploader = require('../../middlewares/uploader');

router.route('/')
    .get(ProductCntrl.find)
    .post(Uploader.single('image'), ProductCntrl.insert)


router.route('/:id')
    .get(ProductCntrl.findById)
    .put(ProductCntrl.update)
    .delete(ProductCntrl.remove)


module.exports = router;