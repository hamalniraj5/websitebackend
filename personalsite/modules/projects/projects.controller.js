const ProductQuery = require('../projects/projects.query');
const fs = require('fs');
const path = require('path');

function find(req, res, next) {
    var condition = {};
    ProductQuery
        .find(condition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function insert(req, res, next) {
    console.log("req body>>>>>>>>>", req.body);
    console.log("req file>>>>>>>>>", req.file);
    var data = req.body;

    if (req.fileErr) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }
    if (req.file) {
        var mimeType = req.file.mimetype.split('/')[0];
        if (mimeType != 'image') {

            fs.unlink(path.join(process.cwd(), 'files/images/' + req.file.filename), function (err, done) {
                if (err) {
                    console.log("error in deketing file");
                } else {
                    console.log("file deleted");

                }
            })
            return next({
                msg: "Inavlid File Format",
                status: 400
            })
        }
        data.image = req.file.filename;
    }

    ProductQuery
        .insert(data)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })

}
function findById(req, res, next) {
    var condition = { _id: req.params.id };
    ProductQuery
        .find(condition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function update(req, res, next) {
    ProductQuery
        .update(req.params.id, req.body)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

module.exports = {
    find,
    insert,
    findById,
    update,
    remove
}