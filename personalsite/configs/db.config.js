const mongodb = require('mongodb');
const Client = mongodb.MongoClient;
const conxnURL = 'mongodb://127.0.0.1:27017';
const dbName = 'personalsite';

module.exports = {
    Client,
    conxnURL,
    dbName
}