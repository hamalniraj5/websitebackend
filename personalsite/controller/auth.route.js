var express = require('express');
var router = express.Router();
const UserModel = require('../models/user.model');
const passwordHash = require('password-hash');
const JWT = require('jsonwebtoken');
const config = require('../configs/index');

function createToken(data) {
    var token = JWT.sign(data, config.JWT_secret);
    return token;
}



router.post('/adminlogin', function (req, res, next) {
    UserModel.findOne({
        username: req.body.username
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            var isMatch = passwordHash.verify(req.body.password, user.password);
            if (isMatch) {
                var token = createToken({
                    name: user.username,
                    _id: user._id
                })
                res.json({
                    user,
                    token
                });
            }
            else {
                next({
                    msg: "Invalid Password"
                })
            }
        }
        else {
            next({
                msg: "Invalid Username"
            })
        }
    })
})

router.post('/adminregister', function (req, res, next) {
    const data = req.body;
    const newUSer = new UserModel({});

    if (data.email)
        newUSer.email = data.email;
    if (data.username)
        newUSer.username = data.username;
    if (data.password)
        newUSer.password = data.password;

    newUSer.password = passwordHash.generate(req.body.password);

    newUSer
        .save()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
})

module.exports = router;