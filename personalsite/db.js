const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');
mongoose.connect(dbConfig.conxnURL + '/' + dbConfig.dbName,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function (err, done) {
        if (err) {
            console.log("db connection failed");    
        } else {
            console.log("database connection successfull");
        }
    });