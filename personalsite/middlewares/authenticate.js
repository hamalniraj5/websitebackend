const JWT = require('jsonwebtoken');
const config = require('../configs');
const ProjectModel = require('../modules/projects/projects.model');

module.exports = function (req, res, next) {
    let token;
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    }
    if (req.headers['authorization']) {
        token = req.headers['authorization'];
    }
    if (req.headers['token']) {
        token = req.headers['token'];
    }
    if (req.query.token) {
        token = req.query.token;
    }
    if (token) {
        // var JWTToken = token.spilit(' ')[1];
        JWT.verify(token, config.JWT_secret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            console.log('decodded value', decoded);
            return next();

        })
    }
    else {
        next({
            msg: 'Token Not Provided',
            status: 400
        })
    }
}