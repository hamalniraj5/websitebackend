const multer = require('multer');

const mystorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

function filter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType === 'image') {
        cb(null, true)
    }
    else {
        cb(null, false)
        req.fileErr = true
    }
}
var upload = multer({
    storage: mystorage,
    fileFilter: filter
})
module.exports = upload;